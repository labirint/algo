package ru.ekaerovets.tree.impl;

import ru.ekaerovets.tree.BinaryTree;
import ru.ekaerovets.utils.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author karyakin dmitry
 *         date 20.03.18.
 */
public class UnbalancedTree<K extends Comparable<K>, V> implements BinaryTree<K, V> {

    private Node<K, V> root = null;

    @Override
    public V put(K key, V value) {
        if (root == null) {
            root = new Node<>(key, value);
            return null;
        }
        Node<K, V> node = root;
        while (true) {
            int compare = key.compareTo(node.key);
            if (compare < 0) {
                if (node.left == null) {
                    node.left = new Node<>(key, value);
                    return null;
                }
                node = node.left;
            } else if (compare == 0) {
                V oldValue = node.value;
                node.value = value;
                return oldValue;
            } else {
                if (node.right == null) {
                    node.right = new Node<>(key, value);
                    return null;
                }
                node = node.right;
            }
        }
    }

    @Override
    public V find(K key) {
        Node<K, V> node = root;
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare < 0)
                node = node.left;
            else if (compare > 0)
                node = node.right;
            else
                return node.value;
        }
        return null;
    }

    @Override
    public V remove(K key) {
        Node<K, V> node = root;
        Node<K, V> parent = null;
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare < 0) {
                parent = node;
                node = node.left;
            }
            else if (compare > 0) {
                parent = node;
                node = node.right;
            } else
                break;
        }
        if (node == null)
            return null;

        // node is the node to be deleted
        // parent is node's parent or null if the node is root
        V toReturn = node.value;

        Node<K, V> leftSubtree = node.left;
        // if the node doesn't have left child, just remove it and fix links
        if (leftSubtree == null) {
            relinkNode(parent, node, node.right);
        } else {
            if (leftSubtree.right == null) {
                relinkNode(parent, node, leftSubtree);
                leftSubtree.right = node.right;
            } else {
                // find the max node, unlink it and put instead of node
                Node unlinkParent = leftSubtree;
                Node<K, V> maxRigth = leftSubtree.right;
                while (maxRigth.right != null) {
                    unlinkParent = maxRigth;
                    maxRigth = maxRigth.right;
                }

                unlinkParent.right = maxRigth.left;
                maxRigth.left = node.left;
                maxRigth.right = node.right;
                relinkNode(parent, node, maxRigth);
            }

        }

        return toReturn;
    }

    @Override
    public List<Pair<K, V>> traverse() {
        List<Pair<K, V>> result = new ArrayList<>();
        if (root != null)
            traverseFromNode(root, result);
        return result;
    }


    private void traverseFromNode(Node<K, V> node, List<Pair<K, V>> result) {
        if (node.left != null)
            traverseFromNode(node.left, result);
        result.add(new Pair<>(node.key, node.value));
        if (node.right != null)
            traverseFromNode(node.right, result);
    }

    private void relinkNode(Node<K, V> parent, Node<K, V> oldChild, Node<K, V> newChild) {
        if (parent == null) {
            root = newChild;
        } else {
            if (parent.left == oldChild)
                parent.left = newChild;
            else
                parent.right = newChild;
        }
    }

    private static class Node<K, V> {
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

}
