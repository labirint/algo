package ru.ekaerovets.tree.impl;

import ru.ekaerovets.tree.BinaryTree;
import ru.ekaerovets.utils.Pair;

import java.util.List;

/**
 * @author karyakin dmitry
 *         date 20.03.18.
 */
public class AvlTree<K extends Comparable<K>, V> implements BinaryTree<K, V> {

    private Node<K, V> root;

    private Node<K, V>[] stack = new Node[50];
    private int stackPtr = 0;

    @Override
    public V put(K key, V value) {
        if (root == null) {
            root = new Node<>(key, value);
            return null;
        }
        stackPtr = 0;
        Node<K, V> node = root;
        while (true) {
            stack[stackPtr++] = node;
            int compare = node.key.compareTo(key);
            if (compare < 0) {
                if (node.left != null) {
                    node = node.left;
                    continue;
                }
                put0(key, value, node, true);
                break;
            } else if (compare > 0) {
                if (node.right != null) {
                    node = node.right;
                    continue;
                }
                put0(key, value, node, false);
                break;
            } else {
                V oldValue = node.value;
                node.value = value;
                return oldValue;
            }
        }
        assert (checkTree());
        return null;
    }

    // stack contains all the nodes from root to the current
    // stackPtr points at the element after target
    private void put0(K key, V value, Node<K, V> target, boolean left) {
        stackPtr--; // stackPtr points at target
        assert(stack[stackPtr] == target);
        boolean heightIncremented = false;
        if (left) {
            target.left = new Node<>(key, value);
            assert (target.balance == -1 || target.balance == 0);
            target.balance++;

            if (target.balance == 1)
                heightIncremented = true;
        } else {
            target.right = new Node<>(key, value);
            assert (target.balance == 1 || target.balance == 0);
            target.balance--;
            if (target.balance == -1)
                heightIncremented = true;
        }
        while (heightIncremented) {
            // if target is the root node, no rebalance needed
            if (stackPtr == 0)
                return;
            Node<K, V> parent = stack[--stackPtr];
            int change;
            if (parent.left == target) {
                change = 1; // left node got bigger
            } else {
                assert (parent.right == target); // check that target is the child of parent
                change = -1;
            }
            if (parent.balance + change == 2) {
                // the left subtree got too big
                // parent balance must be 1, which ensures that we have left part
                Node<K, V> leftTree = parent.left;
                assert (leftTree.balance != 0);
                if (leftTree.balance == 1) {
                    // small right rotation
                    parent.left = leftTree.right;
                    leftTree.right = parent;
                    parent.balance = 0;
                    leftTree.balance = 0;
                    changeParentAfterRotation(parent, leftTree);
                    return;
                } else {
                    // leftTree.balance == -1, thus we have right part
                    Node<K, V> lr = leftTree.right;
                    assert (lr.balance != 0);
                    leftTree.right = lr.left;
                    parent.left = lr.right;
                    lr.left = leftTree;
                    lr.right = parent;
                    leftTree.balance = lr.balance == -1 ? 1 : 0;
                    parent.balance = lr.balance == -1 ? 0 : -1;
                    lr.balance = 0;
                    changeParentAfterRotation(parent, lr);
                    return;
                }
            } else if (parent.balance + change == -2) {
                // the right subtree got too big
                // parent balance must be -1, which ensures that we have right part
                Node<K, V> rightTree = parent.right;
                assert (rightTree.balance != 0);
                if (rightTree.balance == -1) {
                    // small left rotation
                    parent.right = rightTree.left;
                    rightTree.left = parent;
                    parent.balance = 0;
                    rightTree.balance = 0;
                    changeParentAfterRotation(parent, rightTree);
                    return;
                } else {
                    // rightTree.balance == 1, thus we have left part
                    Node<K, V> rl = rightTree.left;
                    assert (rl.balance != 0);
                    rightTree.left = rl.right;
                    parent.right = rl.left;
                    rl.right = rightTree;
                    rl.left = parent;
                    rightTree.balance = rl.balance == 1 ? -1 : 0;
                    parent.balance = rl.balance == 1 ? 0 : 1;
                    rl.balance = 0;
                    changeParentAfterRotation(parent, rl);
                    return;
                }
            }
        }
    }

    private void changeParentAfterRotation(Node<K, V> oldParent, Node<K, V> newParent) {
        if (stackPtr == 0) {
            assert(oldParent == root);
            root = newParent;
        } else {
            Node<K, V> parent = stack[--stackPtr];
            if (parent.left == oldParent) {
                parent.left = newParent;
            } else {
                assert (parent.right == oldParent);
                parent.right = newParent;
            }
        }
    }

    // returns true if the tree is correct
    private boolean checkTree() {
        return (root == null) || checkNode(root) != -1;
    }

    // returns -1 if there is a problem
    // otherwise returns node height
    private int checkNode(Node<K, V> node) {
        int leftH = 0;
        int rightH = 0;
        if (node.left != null) {
            leftH = checkNode(node.left);
            if (leftH == -1)
                return -1;
        }
        if (node.right != null) {
            rightH = checkNode(node.right);
            if (rightH == -1)
                return -1;
        }
        int balance = leftH - rightH;
        if (balance < -1 || balance > 1 || balance != node.balance) {
            return -1;
        }
        return leftH > rightH ? leftH + 1 : rightH + 1;
    }

    @Override
    public V find(K key) {
        Node<K, V> node = root;
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare < 0)
                node = node.left;
            else if (compare > 0)
                node = node.right;
            else
                return node.value;
        }
        return null;
    }

    @Override
    public V remove(K key) {
        return null;
    }

    @Override
    public List<Pair<K, V>> traverse() {
        return null;
    }

    private static class Node<K, V> {

        private K key;
        private V value;
        private Node<K, V> left;
        private Node<K, V> right;
        private int balance; // left.height - right.height

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

}
