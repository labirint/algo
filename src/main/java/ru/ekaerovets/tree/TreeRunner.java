package ru.ekaerovets.tree;

import ru.ekaerovets.tree.impl.AvlTree;
import ru.ekaerovets.tree.impl.UnbalancedTree;
import ru.ekaerovets.utils.Pair;

import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * @author karyakin dmitry
 *         date 20.03.18.
 */
public class TreeRunner {

    public static void main(String[] args) {

        BinaryTree<Integer, Integer> testedTree = new AvlTree<>();
        BinaryTree<Integer, Integer> checkTree = new JavaTree<>();

        Random rnd = new Random(0x12345);

        for (int i = 0; i < 10000; i++) {
            Integer op = rnd.nextInt(3);
            Integer key = rnd.nextInt(100);
            Integer value = rnd.nextInt(100);
            Integer expected = null;
            Integer actual = null;
            switch (op) {
                case 0:
                    System.out.println("put " + key + " = " + value);
                    expected = checkTree.put(key, value);
                    actual = testedTree.put(key, value);
                    break;
                case 1:
                    System.out.println("find " + key);

                    expected = checkTree.find(key);
                    actual = testedTree.find(key);
                    break;
                case 2:
                    System.out.println("remove " + key);

                    expected = checkTree.remove(key);
                    actual = testedTree.remove(key);
                    break;
            }
            if (!Objects.equals(actual, expected))
                System.out.println("Alarm expected " + expected + " but received " + actual);

            List<Pair<Integer, Integer>> check = checkTree.traverse();
            List<Pair<Integer, Integer>> test = testedTree.traverse();
            if (!check.equals(test))
                System.out.println("!!!!!!!!!!!!!!! Unequal");

        }

    }

}
