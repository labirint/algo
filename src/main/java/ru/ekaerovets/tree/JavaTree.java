package ru.ekaerovets.tree;

import ru.ekaerovets.utils.Pair;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author karyakin dmitry
 *         date 20.03.18.
 */
public class JavaTree<K extends Comparable<K>, V> implements BinaryTree<K, V> {

    private Map<K, V> map = new TreeMap<>();

    @Override
    public V put(K key, V value) {
        return map.put(key, value);
    }

    @Override
    public V find(K key) {
        return map.get(key);
    }

    @Override
    public V remove(K key) {
        return map.remove(key);
    }

    @Override
    public List<Pair<K, V>> traverse() {
        return map.entrySet().stream().map((item) -> new Pair<>(item.getKey(), item.getValue())).collect(Collectors.toList());
    }
}
