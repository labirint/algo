package ru.ekaerovets.tree;

import ru.ekaerovets.utils.Pair;

import java.util.List;

/**
 * @author karyakin dmitry
 *         date 20.03.18.
 */
public interface BinaryTree<K extends Comparable<K>, V> {

    V put(K key, V value);

    V find(K key);

    V remove(K key);

    List<Pair<K, V>> traverse();

}
