package ru.ekaerovets.tree;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ru.ekaerovets.tree.impl.AvlTree;
import ru.ekaerovets.tree.impl.UnbalancedTree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * @author labirint
 *         date 20.03.18
 */
@RunWith(Parameterized.class)
public class TreeTest {

    private BinaryTree<Integer, Integer> tree;


    @Parameterized.Parameter
    public Class clazz;

    @Parameterized.Parameters(name = "{index}: Impl class: {0}")
    public static Collection<Class<?>> getImpls() {
        List<Class<?>> result = new ArrayList<>();
        result.add(JavaTree.class);
        result.add(UnbalancedTree.class);
        result.add(AvlTree.class);
        return result;
    }

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws ReflectiveOperationException {
        tree = (BinaryTree<Integer, Integer>) clazz.getConstructor().newInstance();
    }


    @Test
    public void testSimple() {
        testFind(10, null);
        testPut(10, 333, null);
        testPut(10, 117, 333);
        testRemove(10, 117);
    }


    @Test
    public void simpleDeletions() {
        testPut(30, 330, null);
        testPut(40, 400, null);
        testPut(35, 350, null);
        testRemove(40, 400);
        testFind(35, 350);
        testRemove(30, 330);
        testFind(35, 350);
    }

    @Test
    public void emptyTest() {
        testFind(10, null);
        testRemove(30, null);
    }

    @Test
    public void bigRandom() {

        Integer[] model = new Integer[100];
        Random rnd = new Random(0x12345);

        for (int i = 0; i < 10000; i++) {
            Integer op = rnd.nextInt(3);
            Integer key = rnd.nextInt(100);
            Integer value = rnd.nextInt(100);
            Integer expected = null;
            Integer actual = null;
            switch (op) {
                case 0: // put
                    actual = tree.put(key, value);
                    expected = model[key];
                    model[key] = value;
                    break;
                case 1: // find
                    actual = tree.find(key);
                    expected = model[key];
                    break;
                case 2: // remove
                    actual = tree.remove(key);
                    expected = model[key];
                    model[key] = null;
                    break;
            }
            Assert.assertEquals(actual, expected);
        }
    }

    @Test
    public void unbalancedRelinkTest() {
        testPut(40, 40, null);
        testPut(30, 30, null);
        testPut(33, 33, null);
        testPut(10, 10, null);
        testPut(7, 7, null);
        testPut(15, 15, null);
        testPut(22, 22, null);
        testPut(17, 17, null);
        testPut(16, 16, null);
        testPut(19, 19, null);
        testRemove(30, 30);
        testFind(11, null);
        testFind(33, 33);
        testFind(10, 10);
        testFind(22, 22);
        testFind(17, 17);
        testFind(19, 19);
    }

    @Test
    public void unbalancedDeletion2() {
        testPut(55, 55, null);
        testPut(49, 49, null);
        testPut(52, 52, null);
        testPut(51, 51, null);
        testPut(54, 54, null);
        testRemove(52, 52);
        testFind(54, 54);
        testFind(52, null);
        testFind(51, 51);
    }

    private void testPut(Integer key, Integer value, Integer expected) {
        Assert.assertEquals(tree.put(key, value), expected);
    }

    private void testFind(Integer key, Integer expected) {
        Assert.assertEquals(tree.find(key), expected);
    }

    private void testRemove(Integer key, Integer expected) {
        Assert.assertEquals(tree.remove(key), expected);
    }

}
